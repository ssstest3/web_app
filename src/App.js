import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import MainPage from "./components/layout/MainPage";

import "./App.css";

//redux
import { Provider } from "react-redux";
import store from "./store";

const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <MainPage />
      </Router>
    </Provider>
  );
};

export default App;
