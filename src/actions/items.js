import { GET_CARDS_SUCCESS, GET_CARDS_FAIL } from "./type";
import axios from "axios";

// load user

export const getItems = ({ start, size }) => async (dispatch) => {
  try {
    let res = await axios.get(
      "http://www.mocky.io/v2/5ed68221340000480106dae9"
    );
    let joinArray = [];
    joinArray.push(...res.data, ...res.data, ...res.data);

    dispatch({
      type: GET_CARDS_SUCCESS,
      payload: joinArray.slice(start - 1, size),
    });
  } catch (error) {
    dispatch({
      type: GET_CARDS_FAIL,
    });
  }
};
