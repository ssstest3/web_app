import { GET_CARDS_SUCCESS, GET_CARDS_FAIL } from "../actions/type";

const initialState = {
  loading: true,
  items: [],
  hasMore: false,
  error: false,
};

export default function (state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case GET_CARDS_SUCCESS:
      return {
        ...state,
        loading: false,
        items: payload,
        hasMore: payload.length > 0,
      };
    case GET_CARDS_FAIL:
      return {
        ...state,
        error: true,
        loading: false,
      };

    default:
      return state;
  }
}
