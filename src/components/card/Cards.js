import React, { Fragment } from "react";
import CardMedia from "./CardItem";

const Cards = ({ items }) => (
  <Fragment>
    {items.map((item, index) => (
      <a style={{ textDecoration: "none" }} href={item.landingPage}>
        <CardMedia item={item} id={index} />
      </a>
    ))}
  </Fragment>
);

export default Cards;
