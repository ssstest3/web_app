import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import FavoriteBorderOutlinedIcon from "@material-ui/icons/FavoriteBorderOutlined";
import FavoriteIcon from "@material-ui/icons/Favorite";
import { IconButton } from "@material-ui/core";
import {
  RatingAndReview,
  Discount,
} from "../../components/common/commoncomponents";

const useStyles = makeStyles({
  root: {
    margin: "5px",
    width: "350px",
  },

  media: {
    height: "450px",
  },
});

export default function MediaCard({ item }) {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={item.imgSrc}
          title="Contemplative Reptile"
        >
          {item.bestSeller ? (
            <span
              style={{
                height: "80px",
                width: "150px",
                background: "#008539",
                color: "white",
                padding: "20px 10px 5px 20px",
              }}
            >
              BEST SELLER
            </span>
          ) : null}
          <IconButton style={{ float: "right" }}>
            <FavoriteBorderOutlinedIcon fontSize="large" />
            {/* <FavoriteIcon
              fontSize="large"
              style={{
                marginTop: "-5px",
                color: "#008539",
              }}
            /> */}
          </IconButton>
        </CardMedia>
        <CardContent style={{ textAlign: "center" }}>
          <Typography noWrap="true" variant="h6" gutterBottom>
            {item.title}
          </Typography>

          <Typography variant="h4" gutterBottom>
            <span>&#8377;</span>
            {item.sellingPrice}
          </Typography>
          <Discount
            sellingPrice={item.sellingPrice}
            discount={item.discountPercentage}
          />

          <RatingAndReview
            ratings={item.ratingCount}
            reviews={item.reviewCount}
          />
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
