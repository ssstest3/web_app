import React, { useState, Fragment, useEffect } from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import Box from "@material-ui/core/Box";
import "../../style/MainPage.css";
import Menubar from "./MenuBar";
import Cards from "../card/Cards";
import { connect } from "react-redux";
import { getItems } from "../../actions/items";
import Spinner from "./Spinner";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    background: "#008539",
    boxShadow: "none",
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: "flex-end",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
  logo: {
    marginRight: theme.spacing(2),
    backgroundImage: `url(${"https://i7.fnp.com/assets/images/new-fnplogo.png"})`,
  },
}));
const MainPage = ({ loading, items, getItems }) => {
  const [open, setOpen] = useState(false);
  const [size, setSize] = useState(10);
  const [start, setStart] = useState(1);

  useEffect(() => {
    getItems({ start, size });
  }, [getItems]);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };
  const theme = useTheme();
  const classes = useStyles();
  return (
    <Fragment>
      {loading ? (
        <Spinner />
      ) : (
        <div className={classes.root}>
          <CssBaseline />
          <AppBar
            position="fixed"
            className={clsx(classes.appBar, {
              [classes.appBarShift]: open,
            })}
          >
            <Toolbar>
              <IconButton
                color="inherit"
                aria-label="open drawer"
                onClick={handleDrawerOpen}
                edge="start"
                className={clsx(classes.menuButton, open && classes.hide)}
              >
                <MenuIcon />
              </IconButton>
              <div
                style={{
                  margin: "0",
                  padding: "0",
                  background: "white",
                  width: "204px",
                }}
              >
                <a href="/">
                  <img
                    src="https://i7.fnp.com/assets/images/new-fnplogo.png"
                    alt="abc"
                    style={{
                      margin: "0",
                      width: "200px",
                      height: "50px",
                    }}
                  />
                </a>
              </div>
            </Toolbar>
          </AppBar>
          <Drawer
            className={classes.drawer}
            variant="persistent"
            anchor="left"
            open={open}
            classes={{
              paper: classes.drawerPaper,
            }}
          >
            <div className={classes.drawerHeader}>
              <IconButton onClick={handleDrawerClose}>
                {theme.direction === "ltr" ? (
                  <ChevronLeftIcon />
                ) : (
                  <ChevronRightIcon />
                )}
              </IconButton>
            </div>
            <Divider />

            <List>
              <Menubar />
            </List>
          </Drawer>
          <main
            className={clsx(classes.content, {
              [classes.contentShift]: open,
            })}
          >
            <div className={classes.drawerHeader} />
            <div style={{ width: "100%", margin: "auto", marginTop: "5px" }}>
              <Box
                display="flex"
                style={{ flexWrap: "wrap", marginTop: "5px" }}
              >
                <Cards items={items} />
              </Box>
            </div>
          </main>
        </div>
      )}
    </Fragment>
  );
};

MainPage.propTypes = {
  getItems: PropTypes.func.isRequired,
  items: PropTypes.array.isRequired,
  loaing: PropTypes.bool.isRequired,
};
const mapStateToProps = (state) => ({
  items: state.items.items,
  loading: state.items.loading,
});

export default connect(mapStateToProps, { getItems })(MainPage);
