import React, { Fragment } from "react";
import StarRatings from "react-star-ratings";
import Typography from "@material-ui/core/Typography";

export function RatingAndReview({ ratings, reviews }) {
  return (
    <Fragment>
      <span style={{ color: "#fcba03" }}>
        {" "}
        {ratings}{" "}
        <StarRatings
          rating={ratings}
          starRatedColor="#fcba03"
          starDimension="15px"
          starSpacing="-5px"
        />{" "}
      </span>
      <Typography variant="subtitle1" gutterBottom>
        {" "}
        {reviews} Reviews{" "}
      </Typography>
    </Fragment>
  );
}

export function Discount({ sellingPrice, discount }) {
  return (
    <Fragment>
      <Typography style={{ color: "#008539" }}>
        {discount ? (
          <Fragment>
            {discount}% OFF
            <span style={{ color: "#000" }}> {"  "} &#8377; </span>
            <strike style={{ color: "#000" }}>
              {" "}
              {Math.ceil(sellingPrice / ((100 - discount) / 100))}{" "}
            </strike>
          </Fragment>
        ) : (
          <div
            style={{
              height: "24px",
            }}
          />
        )}
      </Typography>
    </Fragment>
  );
}
